﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace pl2fm
{
    public partial class Form1 : Form
    {
        public Form1()
        {        
            InitializeComponent();
            comboBox1.SelectedIndex = 0;
            label4.Visible = false;
        }

        double[,] matriz;
        string[] basicas;
        int[] basi;
        string[] arriba;
        TextBox[,] tb;
        Label[,] lb;
        ComboBox[] cb;
        int vari = 0;
        int restr = 0;
        int M = 1000;
        int col2;
        bool[] R;//sirve para decir en que restriccion hay una R
        int cR = 0, cS = 0;

        private void button1_Click(object sender, EventArgs e)
        {
            
            cancelar.Enabled = true;
            maskedrest.Enabled = false;
            maskedvar.Enabled = false;
            button1.Enabled = false;
            //radio2fases.Enabled = false;
            //radiom.Enabled = false;
            try
            {
                vari = int.Parse(maskedvar.Text);
                restr = int.Parse(maskedrest.Text);
            }
            catch (Exception j)
            {
                MessageBox.Show("Debe llenar los campos", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Console.WriteLine("An error occurred: '{0}'", j);
                cancelar.PerformClick();
                return;
            }

            if ((vari != 0 && restr != 0) || (vari > 0 && restr > 0))
            {

                //MessageBox.Show(vari.ToString(), "hola", MessageBoxButtons.OK, MessageBoxIcon.Information );
                campos(restr, vari);
                
                //button1.Enabled = false;
                panel1.Visible = true;
            }
            else
            {

                MessageBox.Show("Debe llenar los campos validos", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                cancelar.PerformClick();

            }
            
        }

        private void cancelar_Click(object sender, EventArgs e)
        {
            button1.Enabled = true;
            maskedrest.Enabled = true;
            maskedvar.Enabled = true;
            radio2fases.Enabled = true;
            radiom.Enabled = true;
            maskedrest.Text = " ";
            maskedvar.Text = " ";
            cancelar.Enabled = false;
            panel2.Visible = false;
            panel1.Visible = false;
            label10.Text = "";
            label11.Text = "";
              
        }

        private void resetpanel1()
        {

            try
            {
                while (panel1.Controls.Count > 7)
                {
                    foreach (Control item in panel1.Controls)
                    {
                        if (item.Name != "label7" && item.Name != "label4" && item.Name != "label5" && item.Name != "label6" && item.Name != "comboBox1" && item.Name != "label3" && item.Name != "button3")
                        {
                            panel1.Controls.Remove(item);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }

            label7.Text = "";
        }//resetear el panel

        private void campos(int rest, int var)
        {
            resetpanel1();   
            int columnas = var + 1;
            int filas = rest + 1; 
            //Object objetivo = Cobjetivo.SelectedItem;
            //label6.Text = objetivo.ToString() + " Z = ";

            int L = 1;
            tb = new TextBox[filas, columnas];
            lb = new Label[filas, columnas];
            cb = new ComboBox[filas];
            int posX = 150;
            int posY = 45;
            int posXL = 180;
            int posYL = 47;
           
            for (int i = 0; i < filas; i++){
                
                for (int j = 0; j < columnas; j++){

                    lb[i, j] = new Label();
                    tb[i, j] = new TextBox();
                    lb[i, j].Size = new System.Drawing.Size(27, 13);
                    lb[i, j].Location = new System.Drawing.Point(posXL, posYL);
                    tb[i, j].Size = new System.Drawing.Size(30, 20);

                    if (j == columnas - 1)
                    {

                        tb[i, j].Location = new System.Drawing.Point(posX + 65, posY);
                        if (i == 0)
                        {

                            tb[i, j].Text = "0";
                            tb[i, j].Visible = false;
                        }
                    }
                    else
                    {
                    
                
                        tb[i, j].Location = new System.Drawing.Point(posX, posY);
                        
                    }
                        posX += 70;
                        posXL += 70;
                    
                    if (j == columnas - 1)
                    {
                        lb[i, j].Text = "";
         
                     }
                    else if (j == (columnas - 2))
                    {
                            lb[i, j].Text = "X" + (j + 1) + "  ";
                        
                        if (i > 0){
                                cb[L] = new ComboBox();
                                cb[L].Size = new System.Drawing.Size(40, 20);
                                cb[L].Location = new System.Drawing.Point(posXL - 30, posYL - 3);
                                cb[L].DropDownStyle = ComboBoxStyle.DropDownList;
                                cb[L].Items.Add("<=");
                                cb[L].Items.Add("=");
                                cb[L].Items.Add(">=");
                                cb[L].SelectedIndex = 0;
                                panel1.Controls.Add(cb[L]);
                                L++;
       
                            }
                            

                      }
                      else
                    {
                      lb[i, j].Text = "X" + (j + 1) + "+";
                     }
                      
                        panel1.Controls.Add(lb[i, j]);
                        panel1.Controls.Add(tb[i, j]);
                    }
                    posX = 90; posXL = 130; posY += 40; posYL += 42;
                    
                }
            button3.Location = new Point(250, posY);
            }

        private void button3_Click(object sender, EventArgs e)
        {

            if (armarmatriz(restr, vari))
            {
                panel2.Visible = true;
                label10.Text = "";
                label11.Text = "";
                label8.Text = "";
                label9.Text = "";
                mestandar(restr + 1);
                label10.Text="Original:\n";
                printTable(matriz,arriba,label10);
                if(radio2fases.Checked){
                    //llamado al metodo dos fase
                    
                    label9.Location = new System.Drawing.Point(485, 25);
                    label8.Text = "Fase1.";
                    label9.Text = "Fase2.";
                    fase1(matriz);
                }else{
                    label8.Location = new System.Drawing.Point(45, 25);
                    label8.Text = "M = "+M;
                    label9.Location=new System.Drawing.Point(370, 0);
                    label9.Text = "Metodo de la M";
                    tecnicaM(matriz);
                    //// aca el llamado al metodo de la M
                }
                //simple(matriz,label10);
            }
        }

        private bool armarmatriz(int rest, int var)
        {

            int filas = rest+1;
            int columnas = var+1;
            double[,] matrizaux = new double[filas, columnas];

            for (int m = 0; m < filas; m++) { for (int n = 0; n < columnas; n++) { matrizaux[m, n] = 0; } }
            bool vacio = false;
            try
            {
                for (int m = 0; m < filas; m++)
                {
                    for (int n = 0; n < columnas; n++)
                    {
                        if (tb[m, n].Text == "")
                        {
                            m = filas;
                            n = columnas;
                            vacio = true;
                            MessageBox.Show("Debes Llenar todos los campos");
                            return false;
                            
                        }
                        matrizaux[m, n] = double.Parse(tb[m, n].Text);
                    }
                }
            }
            catch (Exception e)
            {
                MessageBox.Show("Solo numeros.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Console.WriteLine("An error occurred: '{0}'", e);
                return false;
            }
            if (!vacio)
            {
               
                int col = filas;
                basicas = new string[col];
                basi = new int[col-1];
                basicas[0] = "Z  ";
                R = new bool[col];
                int[] S = new int[col];//sim en la fila hay una s
                cR = 0; 
                cS = 0;
                for (int i = 0; i < col; i++) { R[i] = false; S[i] = 0; }
                for (int j = 1; j < col; j++)
                {
                    if (cb[j].SelectedIndex == 0)
                    {
                        S[j] = 1;
                        cS += 1;
                        basicas[j] = "S" + cS;
                        basi[j-1]=vari+cS;
                    }
                    else if (cb[j].SelectedIndex == 1)
                    {
                        R[j] = true;
                        cR += 1;
                        basicas[j] = "R" + cR;
                        basi[j - 1] = vari + cR;
                    }
                    else
                    {
                        S[j] = -1;
                        R[j] = true;
                        cS += 1;
                        cR += 1;
                        basicas[j] = "R" + cR;
                        basi[j - 1] = vari + cR;
                    }
                }


                col2 = columnas + cS + cR;
                matriz = new double[filas, col2];
                arriba = new string[col2];
                
                for (int i = 0; i < filas; i++) { for (int j = 0; j < col2; j++) { matriz[i, j] = 0; } }
                for (int i = 0; i < filas; i++) { for (int j = 0; j < columnas - 1; j++) { matriz[i, j] = matrizaux[i, j]; } }

                int r1 = columnas - 1 + cS;
                int s2 = columnas - 1;
                int Rcont = 0;
                int Scont = 0;

                for (int i = 0; i < filas; i++) 
                { 
                    if (S[i] != 0) 
                    { 
                        matriz[i, s2 + Scont] = S[i]; 
                        Scont++; 
                    } 
                }
                
                for (int i = 0; i < filas; i++) 
                { 
                    if (R[i]) 
                    { 
                        matriz[i, r1 + Rcont] = 1; 
                        Rcont++; 
                    } 
                }
                for (int i = 0; i < filas; i++) 
                { 
                    matriz[i, col2 - 1] = matrizaux[i, columnas - 1]; 
                }
                
                Rcont = 1;
                Scont = 1;
               
                for (int m = 0; m < (col2); m++) { 
                    if (m < vari) { 
                        arriba[m] = "X" + (m + 1) + " "; 
                    } else { 
                        if (m < (vari + cS)) { 
                            arriba[m] = ("S" + (Scont) + " "); 
                            Scont += 1; 
                        } else { 
                            if (m < (vari + cS + cR)) {
                                arriba[m] = ("R" + (Rcont) + " "); 
                                Rcont += 1; 
                            } else { 
                                arriba[m] = "Sol"; 
                            } 
                        } 
                    } 
                }//las de arriba
            

            }
            return true;
        }

        private void printTable(double[,] tableu2,string[]arri,Label labe)
        {

            labe.Text += ("-------------------------------\n");
            labe.Text += "     |";
            for(int i=0;i<arri.Length;i++)
            {
                
                labe.Text += "*\t     *" + arri[i];

            }
            labe.Text += "*\t     *\n";


            for (int m = 0; m <= restr; m++)
            {
                
                labe.Text += (basicas[m] + ":|         \t");
                for (int n = 0; n < (arri.Length); n++)
                {
                    
                    labe.Text += Math.Round(tableu2[m, n], 2) + "|\t         |";
                }
                
                labe.Text += "\n";
            }
            labe.Text += ("-----------------------------\n\n");
        }

        private void mestandar(int filas)
        {
            label4.Visible = true;
            label7.Text = "";
            if (comboBox1.SelectedIndex == 0)
            {
                label7.Text = "Max Z = ";
            }
            else {
                label7.Text = "Min Z = ";
            }
            label7.Visible = true;


            for (int m = 0; m < filas; m++)
            {
                if (m == 1)
                    { 
                        //label7.Text += "=" + matriz[m, n] + "\n\n";
                        label7.Text += "S.A:\n\n"; 
                    }

                for (int n = 0; n < col2; n++)
                {
                    if (n < col2 - 1)
                    {
                        if (matriz[m, n] > 0)
                        {

                            label7.Text += " + " + matriz[m, n] + arriba[n];

                        }
                        else
                        {
                            if (matriz[m, n] < 0)
                            {

                                label7.Text +="  "+ matriz[m, n] + arriba[n];
                            }
                            else {
                                label7.Text += "  \t     \t  \t \t\t\t\t";
                            }

                        }
                    }
                    else {
                        if (m > 0)
                        {
                            label7.Text += " = " + matriz[m, n];
                        }
                    }

                }
                label7.Text += "\n";
            }

        }

        private void fase1(double[,] mtrix) {
            
            double[] reglon0aux=new double[(col2-cR)];
            string[] arrinr = new string[col2-cR];
            double result = 0;
            int combo = comboBox1.SelectedIndex;
            comboBox1.SelectedIndex = 1;
            for (int i = 0; i < col2; i++)
            {
                if(i<(col2-cR)-1){
                    reglon0aux[i] = mtrix[0, i];
                    arrinr[i]=arriba[i];
                }else{
                    
                    i = col2 - 1;
                    reglon0aux[(col2-cR)-1] = mtrix[0, i];
                    arrinr[col2-cR-1] = arriba[i];

                }
                
            }
            
            
            
            for (int n = 0; n < col2; n++)
            {
                for (int m = 0; m < restr+1; m++)
                {
                    if (R[m])
                    {
                        result += mtrix[m, n];   
                    }
                    if (m == restr && result != 0 )
                    {
                       
                        mtrix[0, n] = result;
  
                    }

                }
                if (n == (col2-cR)-3)
                {
                    
                    n = col2 - 2;
                  
                }


                result = 0;

            }
           
            printTable(mtrix,arriba,label10);
            simple(mtrix,label10,arriba);
            comboBox1.SelectedIndex = combo;
            if (cR > 0)
            {
                fase2(mtrix, reglon0aux, arrinr);
            }
            else {
                for (int m = 1; m < restr + 1;m++ ) {
                    label11.Text += basicas[m] + " = " +Math.Round( mtrix[m, col2 - 1],3)+"\n";
                }
                
                label11.Text = "Solucion: " +Math.Round( (mtrix[0, col2 - 1]),3); 
            }
        //label11.Text+=mtrix[0,col2-1];
        }

        private void fase2(double[,] mtrix, double[] reglon0,string []arris) {
            if (Math.Round (mtrix[0, col2-1], 3)== 0.00)
            {
                
                int nr = col2 - cR;
                int filas=restr+1;
                
                double[,] matrix2 = new double[filas,nr];
                label11.Text = "Matriz para fase 2.\n";
                for (int m = 0; m < filas; m++)
                {
                    for (int n = 0; n < col2; n++)
                    {
                        if (n < (nr-1))
                        {
                            matrix2[m, n] =  mtrix[m, n];
                         }
                        else{
                           
                            n = col2-1;
                            matrix2[m, nr-1] = mtrix[m, n];

                            }
                    }
                }
                for (int m = 0; m < basi.Length; m++) {
                    //MessageBox.Show(basi[m].ToString()+"reglon"+reglon0[basi[m]].ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    if(reglon0[basi[m]]!=0){

                        for (int n = 0; n < nr; n++) {
                            if(n!=basi[m]){
                                matrix2[0, n] += reglon0[basi[m]] * matrix2[m + 1, n];}
                        }
                    
                    }
                
                }
                printTable(matrix2,arris,label11);
                simple(matrix2,label11,arris);
                for (int m = 1; m < restr + 1; m++)
                {
                    label11.Text += basicas[m] + " = " +Math.Round( matrix2[m, nr-1],3)+"\n";
                }
                label11.Text += "Solucion: " +Math.Round( (matrix2[0, nr-1]),3); 
            }
            else {
                label11.Text = "El problema no tiene solucion.";
            }
        }

        private void tecnicaM(double[,] mtrix){

            int BM;
            if (comboBox1.SelectedIndex == 0)
            {

                BM = M * -1;

            }
            else {
                BM = M;
            }

            for (int n = 0; n < col2;n++ ) {

                mtrix[0, n] = mtrix[0, n] * -1;
            
            }
            
            for (int m = 0; m < restr+1; m++)
            {
                 if (R[m])
                 {
                     for (int n = 0; n < col2; n++)
                     {
                         if (n == (col2 - cR) - 2)
                         {
                             n = col2 - 1;
                         } 
                         mtrix[0,n]+=Math.Round(BM*mtrix[m,n],6);
                     
                     }
                    
                }
            }
            printTable(mtrix,arriba,label10);
            simple(mtrix,label10,arriba);
            label11.Text = "Matriz final\n\n";
            printTable(mtrix,arriba,label11);
            for (int m = 1; m < restr + 1; m++)
            {
                label11.Text += basicas[m] + " = "+Math.Round(mtrix[m, col2 - 1],3)+"\n";
            }
            label11.Text += "Solucion: " +Math.Round((mtrix[0, col2 - 1]),3); 
            
        }

        private void simple(double [,]tabla1,Label labe,string[] arri) {
            
            int ite=0;
            int possale,posentra;
            double pivofila,pivocol,aux;
            bool opt = false;
            bool acot  = false;
            string entra,sale;
            int col = arri.Length;

            while ((!opt) && (!acot))
            {

                pivocol = 0.0;
                pivofila = 999999999999999.9;
                posentra = -1;
                possale = 0;
                entra = "";
                sale = "";
                aux = -1;

                switch (comboBox1.SelectedIndex) { 

                    case 0:
                        for (int n = 0; n < (col - 1); n++)
                        {

                            if (tabla1[0, n] < pivocol)
                            {

                                pivocol = tabla1[0, n];
                                posentra = n;
                                entra = arriba[n];
                                opt = false;
                            }
                        }
                        break;
                    
                    case 1:
                         for (int n = 0; n < (col - 1); n++)
                         {
                             if (tabla1[0, n] > pivocol)
                             {
                                 pivocol = tabla1[0, n];
                                 posentra = n;
                                 entra = arriba[n];
                                 opt = false;
                             }
                         }
                        break;
                
                }
               
                if (pivocol == 0)
                {
                    opt = true;
                }
                      

                if (!opt)
                {

                    for (int m = 1; m < restr + 1; m++)
                    {

                        if (tabla1[m, posentra] > 0)
                        {

                            aux = (tabla1[m, col - 1]) / tabla1[m, posentra];
                            if (aux < pivofila)
                            {

                                pivofila = aux;
                                possale = m;
                                sale = basicas[m];
                                acot = false;
                            }
                        }
                        if (pivofila == 999999999999999.9)
                        {
                            acot = true;
                        }
                    }
                }

                if ((posentra >= 0) && (possale > 0))
                {
                    opt = false;
                    acot = false;
                    ite += 1;
                    labe.Text += "Iteracion: " + ite + "\n";
                    labe.Text += "Entra: " + entra + ", Sale:" + sale + "\n\n";
                    pivotea(tabla1, posentra, possale, col);
                    basicas[possale] = entra;
                    basi[(possale - 1)] = posentra;
                    printTable(tabla1, arri, labe);
                }else{
                    if(acot){
                        labe.Text += "el problema es no acotado por:"+arri[posentra];
                    }
                    
                }
            }
        }
       
        private void pivotea(double[,] tabla,int entra,int sale,int col){
            
            double pivote;
            double z;
            double [] fpivote=new double[col];
            pivote=tabla[sale,entra];

            for (int n=0;n<col; n++){
                
                tabla[sale,n]=Math.Round((tabla[sale,n]/pivote),10);
                fpivote[n]=tabla[sale,n];
      
            }
 
            for(int m=0;m<restr+1;m++){
                z = tabla[m, entra];
                for (int n = 0; n < col;n++ )
                {
                    if (m != sale)
                    {
                        tabla[m,n] = Math.Round((tabla[m,n] - (z * fpivote[n])),10 );
                    }
                }
            }
     

    }


    }
}
